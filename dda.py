from tkinter import *
import math
import tkinter as tk

import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *

CTR = 1
x1 = 0
y1 = 0
x2 = 0
y2 = 0 

verticies = (
    (1, -1, -1),
    (1, 1, -1),
    (-1, 1, -1),
    (-1, -1, -1),
    (1, -1, 1),
    (1, 1, 1),
    (-1, -1, 1),
    (-1, 1, 1)
    )

edges = (
    (0,1),
    (0,3),
    (0,4),
    (2,1),
    (2,3),
    (2,7),
    (6,3),
    (6,4),
    (6,7),
    (5,1),
    (5,4),
    (5,7)
    )

def Cube():
    glBegin(GL_LINES)
    for edge in edges:
        for vertex in edge:
            glVertex3fv(verticies[vertex])
    glEnd()

def leftClick(event):
    global CTR
    global x1
    global y1
    global x2
    global y2
    a=mode.get()

    if a==1:
        if CTR % 2 == 1:
            y1 = event.y
            x1 = event.x
        elif CTR % 2 == 0:
            y2 = event.y
            x2 = event.x

            dx = x2-x1
            dy = y2-y1

            try:
                m = abs(dy/dx)
            except:
                m = 1

            x = x1
            y = y1

            while x != x2 and y != y2:
                img.put("#ffffff", (round(x), round(y)))
                if y1 > y2:
                    y -= m
                elif y1 < y2:
                    y += m

                if x1 > x2:
                    x -= 1
                elif x1 < x2:
                    x += 1
        CTR+=1
            
    if a == 2:
        if CTR % 2 == 1:
            y1 = event.y
            x1 = event.x
        elif CTR % 2 == 0:
            y2 = event.y
            x2 = event.x

            r = math.sqrt(((x1 - x2)**2) + ((y1 - y2)**2))

            o = 0
            while o <= 360:
                x = x1 + r * math.cos(o)
                y = y1 + r * math.sin(o)
                img.put("#ffffff", (abs(round(x)), abs(round(y))))
                o+=1
        CTR+=1    
    
    if a==4 :
        startCube()

def startCube() :
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        glRotatef(1, 3, 1, 1)
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        Cube()
        pygame.display.flip()
        pygame.time.wait(10)

            
  
pygame.init()
display = (800,600)
pygame.display.set_mode(display, DOUBLEBUF|OPENGL)
gluPerspective(45, (display[0]/display[1]), 0.1, 50.0)
glTranslatef(0.0,0.0, -5)

root = Tk()
mode = tk.IntVar()
panjang, lebar = 800, 800
root.geometry(str(panjang)+"x"+str(lebar+50))

canvas = Canvas(root, width=lebar, height=panjang, bg="#000000")
canvas.bind("<Button-1>", leftClick)
canvas.pack()
img = PhotoImage(width=lebar, height=panjang)
canvas.create_image((lebar/2, panjang/2), image=img, state="normal")

Radiobutton(root,text="dda",variable=mode,value=1,indicatoron = 0).place(x="75",y="810")
Radiobutton(root,text="Lingkaran",variable=mode,value=2,indicatoron = 0).place(x="275",y="810")
Radiobutton(root,text="Bersen",variable=mode,value=3,indicatoron = 0).place(x="475",y="810")
Radiobutton(root,text="Rotating Cube",variable=mode,value=4,indicatoron = 0).place(x="675",y="810")

mainloop()